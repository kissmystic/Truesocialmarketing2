/** @type {import('tailwindcss').Config} */
import defaultTheme from 'tailwindcss/defaultTheme';

module.exports = {
  darkMode: ['class'],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
  ],
  theme: {
    screens: {
      xs: '360px',
      ...defaultTheme.screens,
    },
    container: {
      center: true,
    },
    extend: {
      boxShadow: {
        '3xl': '0 1px 15px 0 rgba(62,65,159,.1)',
      },
      keyframes: {
        'accordion-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: 0 },
        },
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
      },
      colors: {
        regularColorBlack: '#66717a',
        regularColorOrange: '#ac8d68',
        h2Color: '#070143',
        h5Color: '#4d4c60',
        borderColor: '#c4c4c4',
        topBar: '#f7f2ee',
        pTextColore: '#595959',
        mobileSidebarBackground: '#1a1729',
      },
    },
  },
  plugins: [require('tailwindcss-animate')],
};
