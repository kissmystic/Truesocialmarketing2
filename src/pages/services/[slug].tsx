import Layout from '@/components/layout/Layout';
import SEO from '@/components/seo';
import { INFO } from '@/constants';
import services from '@/constants/services';
import ServicePage from '@/sections/servicesPage/ServicePage';

export default function Services({ service }: any) {
  const serviceItem = services.find((item) => item.slug === service);

  return (
    <Layout>
      <SEO title={`${serviceItem?.title} | ${INFO.companyName}`} />
      <ServicePage service={serviceItem} {...{ services }} />
    </Layout>
  );
}

export async function getStaticProps({ params }: any) {
  const { slug } = params;

  return {
    props: {
      service: slug,
    },
  };
}

export async function getStaticPaths() {
  const paths = services.map((service) => ({
    params: { slug: service.slug },
  }));

  return {
    paths,
    fallback: false,
  };
}
