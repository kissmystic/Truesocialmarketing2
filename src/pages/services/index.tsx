import Layout from "@/components/layout/Layout";
import SEO from "@/components/seo";
import { INFO } from "@/constants";
import ServicesPage from "@/sections/servicesPage";

export default function Services() {
  return (
    <Layout>
      <SEO title={`Services | ${INFO.companyName}`} />
      <ServicesPage />
    </Layout>
  );
}
