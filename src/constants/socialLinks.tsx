import {
  faFacebook,
  faInstagram,
  faTwitter,
  faGooglePlus,
} from '@fortawesome/free-brands-svg-icons';

export default [
  {
    name: 'Facebook',
    href: '#',
    icon: faFacebook,
  },
  {
    name: 'Twitter',
    href: '#',
    icon: faTwitter,
  },
  {
    name: 'Instagram',
    href: '#',
    icon: faInstagram,
  },
  {
    name: 'Google+',
    href: '#',
    icon: faGooglePlus,
  },
];
