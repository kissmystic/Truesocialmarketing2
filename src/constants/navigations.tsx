export default [
  {
    id: 'home',
    title: 'Home',
    href: '/',
  },
  {
    id: 'about_us',
    title: 'About Us',
    href: '/about_us',
  },
  {
    id: 'project',
    title: 'Project',
    href: '/project',
  },
  {
    id: 'pages',
    title: 'Pages',
    href: '/pages',
  },
  {
    id: 'blog',
    title: 'Blog',
    href: '/blog',
  },
  {
    id: 'contact',
    title: 'Contact',
    href: '/contact',
  },
];
