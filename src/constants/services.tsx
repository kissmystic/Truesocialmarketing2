export default [
  {
    title: 'Floor Plans',
    slug: 'floor-plans',
    iconImg: '/images/services/2.png',
    bulletpoints: [
      'Bathroom Remodeling',
      'Kitchen Remodeling',
      'Roofing',
      'Residential Remodeling',
      'Restoration',
      'Ground up Renovations',
      'Custom Home building',
    ],
    heroImg: '/images/services/service1/2.jpg',
    imgArray: ['/images/services/service1/1.jpg'],
    twoImgsGallery: [
      '/images/services/service1/3.jpg',
      '/images/services/service1/4.jpg',
    ],
    shortDescription:
      'Lacus, etiam sed est eu tempus need Temer diam congue laoret .',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dis posuere amet tincidunt commodo, velit. Ipsum, hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit. Et nisi in libero facilisis sed est. Elit curabitur amet risus bibendum. Posuere et eget orci, tempor enim.
      Hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit.`,
    capabilities: {
      description:
        'Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam',
      list: [
        'Non saed velit dictum quam risus pharetra esta.',
        'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
        'Hac nibh fermentum nisi, platea condimentum cursus.',
        'Massa volutpat odio facilisis purus sit elementum.',
        'Elit curabitur amet risus bibendum.',
      ],
    },
    approach:
      "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat suspendisse aenean tellus augue morbi risus. Sit morbi vitae morbi sed urna sed purus. Orci facilisi eros sed pellentesque. Risus id sed tortor sed scelerisque. Vestibulum elit elementum, magna id viverra non, velit. Pretium, eros, porttitor fusce auctor vitae id. Phasellus scelerisque nibh eleifend vel enim mauris purus. Rutrum vel sem adipiscing nisi vulputate molestie scelerisque molestie ultrices. Eu, fusce vulputate diam interdum morbi ac a.',",
    workProcess: [
      'Non saed velit dictum quam risus pharetra esta.',
      'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
      'Hac nibh fermentum nisi, platea condimentum cursus.',
      'Massa volutpat odio facilisis purus sit elementum.',
    ],
  },
  {
    title: 'Furniture',
    slug: 'furniture',
    iconImg: '/images/services/3.png',
    bulletpoints: [
      'Bathroom Remodeling',
      'Kitchen Remodeling',
      'Roofing',
      'Residential Remodeling',
      'Restoration',
      'Ground up Renovations',
      'Custom Home building',
    ],
    heroImg: '/images/services/service1/2.jpg',
    imgArray: ['/images/services/service1/1.jpg'],
    twoImgsGallery: [
      '/images/services/service1/3.jpg',
      '/images/services/service1/4.jpg',
    ],
    shortDescription:
      'Lacus, etiam sed est eu tempus need Temer diam congue laoret .',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dis posuere amet tincidunt commodo, velit. Ipsum, hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit. Et nisi in libero facilisis sed est. Elit curabitur amet risus bibendum. Posuere et eget orci, tempor enim.
      Hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit.`,
    capabilities: {
      description:
        'Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam',
      list: [
        'Non saed velit dictum quam risus pharetra esta.',
        'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
        'Hac nibh fermentum nisi, platea condimentum cursus.',
        'Massa volutpat odio facilisis purus sit elementum.',
        'Elit curabitur amet risus bibendum.',
      ],
    },
    approach:
      "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat suspendisse aenean tellus augue morbi risus. Sit morbi vitae morbi sed urna sed purus. Orci facilisi eros sed pellentesque. Risus id sed tortor sed scelerisque. Vestibulum elit elementum, magna id viverra non, velit. Pretium, eros, porttitor fusce auctor vitae id. Phasellus scelerisque nibh eleifend vel enim mauris purus. Rutrum vel sem adipiscing nisi vulputate molestie scelerisque molestie ultrices. Eu, fusce vulputate diam interdum morbi ac a.',",
    workProcess: [
      'Non saed velit dictum quam risus pharetra esta.',
      'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
      'Hac nibh fermentum nisi, platea condimentum cursus.',
      'Massa volutpat odio facilisis purus sit elementum.',
    ],
  },
  {
    title: 'Planning',
    slug: 'planning',
    iconImg: '/images/services/4.png',
    bulletpoints: [
      'Bathroom Remodeling',
      'Kitchen Remodeling',
      'Roofing',
      'Residential Remodeling',
      'Restoration',
      'Ground up Renovations',
      'Custom Home building',
    ],
    heroImg: '/images/services/service1/2.jpg',
    imgArray: ['/images/services/service1/1.jpg'],
    twoImgsGallery: [
      '/images/services/service1/3.jpg',
      '/images/services/service1/4.jpg',
    ],
    shortDescription:
      'Lacus, etiam sed est eu tempus need Temer diam congue laoret .',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dis posuere amet tincidunt commodo, velit. Ipsum, hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit. Et nisi in libero facilisis sed est. Elit curabitur amet risus bibendum. Posuere et eget orci, tempor enim.
      Hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit.`,
    capabilities: {
      description:
        'Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam',
      list: [
        'Non saed velit dictum quam risus pharetra esta.',
        'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
        'Hac nibh fermentum nisi, platea condimentum cursus.',
        'Massa volutpat odio facilisis purus sit elementum.',
        'Elit curabitur amet risus bibendum.',
      ],
    },
    approach:
      "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat suspendisse aenean tellus augue morbi risus. Sit morbi vitae morbi sed urna sed purus. Orci facilisi eros sed pellentesque. Risus id sed tortor sed scelerisque. Vestibulum elit elementum, magna id viverra non, velit. Pretium, eros, porttitor fusce auctor vitae id. Phasellus scelerisque nibh eleifend vel enim mauris purus. Rutrum vel sem adipiscing nisi vulputate molestie scelerisque molestie ultrices. Eu, fusce vulputate diam interdum morbi ac a.',",
    workProcess: [
      'Non saed velit dictum quam risus pharetra esta.',
      'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
      'Hac nibh fermentum nisi, platea condimentum cursus.',
      'Massa volutpat odio facilisis purus sit elementum.',
    ],
  },
  {
    title: 'Decor plan',
    slug: 'decore-plan',
    iconImg: '/images/services/5.png',
    bulletpoints: [
      'Bathroom Remodeling',
      'Kitchen Remodeling',
      'Roofing',
      'Residential Remodeling',
      'Restoration',
      'Ground up Renovations',
      'Custom Home building',
    ],
    heroImg: '/images/services/service1/2.jpg',
    imgArray: ['/images/services/service1/1.jpg'],
    twoImgsGallery: [
      '/images/services/service1/3.jpg',
      '/images/services/service1/4.jpg',
    ],
    shortDescription:
      'Lacus, etiam sed est eu tempus need Temer diam congue laoret .',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dis posuere amet tincidunt commodo, velit. Ipsum, hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit. Et nisi in libero facilisis sed est. Elit curabitur amet risus bibendum. Posuere et eget orci, tempor enim.
      Hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit.`,
    capabilities: {
      description:
        'Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam',
      list: [
        'Non saed velit dictum quam risus pharetra esta.',
        'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
        'Hac nibh fermentum nisi, platea condimentum cursus.',
        'Massa volutpat odio facilisis purus sit elementum.',
        'Elit curabitur amet risus bibendum.',
      ],
    },
    approach:
      "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat suspendisse aenean tellus augue morbi risus. Sit morbi vitae morbi sed urna sed purus. Orci facilisi eros sed pellentesque. Risus id sed tortor sed scelerisque. Vestibulum elit elementum, magna id viverra non, velit. Pretium, eros, porttitor fusce auctor vitae id. Phasellus scelerisque nibh eleifend vel enim mauris purus. Rutrum vel sem adipiscing nisi vulputate molestie scelerisque molestie ultrices. Eu, fusce vulputate diam interdum morbi ac a.',",
    workProcess: [
      'Non saed velit dictum quam risus pharetra esta.',
      'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
      'Hac nibh fermentum nisi, platea condimentum cursus.',
      'Massa volutpat odio facilisis purus sit elementum.',
    ],
  },
  {
    title: 'Furniture',
    slug: 'furniture2',
    iconImg: '/images/services/2.png',
    bulletpoints: [
      'Bathroom Remodeling',
      'Kitchen Remodeling',
      'Roofing',
      'Residential Remodeling',
      'Restoration',
      'Ground up Renovations',
      'Custom Home building',
    ],
    heroImg: '/images/services/service1/2.jpg',
    imgArray: ['/images/services/service1/1.jpg'],
    twoImgsGallery: [
      '/images/services/service1/3.jpg',
      '/images/services/service1/4.jpg',
    ],
    shortDescription:
      'Lacus, etiam sed est eu tempus need Temer diam congue laoret .',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dis posuere amet tincidunt commodo, velit. Ipsum, hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit. Et nisi in libero facilisis sed est. Elit curabitur amet risus bibendum. Posuere et eget orci, tempor enim.
      Hac nibh fermentum nisi, platea condimentum cursus velit dui. Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam, elit sit.`,
    capabilities: {
      description:
        'Massa volutpat odio facilisis purus sit elementum. Non, sed velit dictum quam. Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt. Id aliquet duis sollicitudin diam',
      list: [
        'Non saed velit dictum quam risus pharetra esta.',
        'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
        'Hac nibh fermentum nisi, platea condimentum cursus.',
        'Massa volutpat odio facilisis purus sit elementum.',
        'Elit curabitur amet risus bibendum.',
      ],
    },
    approach:
      "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat suspendisse aenean tellus augue morbi risus. Sit morbi vitae morbi sed urna sed purus. Orci facilisi eros sed pellentesque. Risus id sed tortor sed scelerisque. Vestibulum elit elementum, magna id viverra non, velit. Pretium, eros, porttitor fusce auctor vitae id. Phasellus scelerisque nibh eleifend vel enim mauris purus. Rutrum vel sem adipiscing nisi vulputate molestie scelerisque molestie ultrices. Eu, fusce vulputate diam interdum morbi ac a.',",
    workProcess: [
      'Non saed velit dictum quam risus pharetra esta.',
      'Id risus pharetra est, at rhoncus, nec ullamcorper tincidunt.',
      'Hac nibh fermentum nisi, platea condimentum cursus.',
      'Massa volutpat odio facilisis purus sit elementum.',
    ],
  },
];
