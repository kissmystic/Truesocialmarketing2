export const INFO = {
  phone: '+8 (123) 123 4563',
  email: 'avadh@gmail.com',
  address: 'Fna city, LH 3656, USA',
  companyName: 'TSM',
  site: 'https://tsm.com',
  locationUrl: '',
};

export const MetaData = {
  title: '',
  description: '',
};
