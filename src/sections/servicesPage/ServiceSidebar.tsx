import services from '@/constants/services';
import { cn } from '@/utils';
import { faArrowRightLong } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';

const ServiceSidebar = ({ service }: any) => {
  return (
    <div className="grid grid-cols-1 md:grid-cols-1 xl:grid-cols-1 gap-4 sm:gap-6">
      <div className="p-4 lg:p-8">
        <h2
          className="
            mb-4 
            relative
            sm:mb-6 
            text-2xl 
            before:rounded-xl 
            before:bottom-0 
            before:h-1 
            before:absolute 
            before:bg-regularColorOrange 
            before:left-0 
            before:w-14
            pb-5

            after:rounded-xl
            after:bottom-0
            after:h-1
            after:absolute
            after:left-16
            after:w-[80%]
          after:bg-[#f2f2f2]
        ">
          Services
        </h2>
        <div>
          {services.map((item: any, index: number) => (
            <Link
              href={`/services/${item.slug}`}
              className="flex pb-8 text-[16px] text-[#474f62] font-bold">
              <div key={index}>{item.title}</div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ServiceSidebar;
