import Image from '@/components/Image';

const Service = ({ title, description, imageSrc, classes = '' }: any) => {
  return (
    <div
      className={`px-5 py-12 bg-white flex items-start justify-center flex-col max-w-[270px] ${classes}`}>
      <span className="h-20 w-20 flex justify-center items-center mb-5">
        <Image className="w-16 h-16 relative" src={imageSrc} alt="logo" />
      </span>
      <h2 className="pb-3 font-normal text-xl">{title}</h2>
      <p>{description}</p>
    </div>
  );
};

export default Service;
