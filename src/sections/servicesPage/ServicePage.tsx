import Image from '@/components/Image';
import ServiceSidebar from './ServiceSidebar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCheck,
  faEnvelope,
  faLocationDot,
  faPhoneVolume,
} from '@fortawesome/free-solid-svg-icons';
import PageHead from '@/components/pageHead';
import { INFO } from '@/constants';
import formatPhone from '@/utils/formatPhone';
import Link from 'next/link';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination, Navigation, EffectFade } from 'swiper/modules';
import Service from '@/sections/servicesPage/Service';

const ServicePage = ({ service, services }: any) => {
  return (
    <>
      <PageHead
        title={service.title}
        pageName={service.title}
        description={service.description}
        heroImg={service.heroImg}
        hasBreadCrumb
      />
      <section className="padding-y">
        <div className="container">
          <div className="grid grid-cols-1 xl:grid-cols-12 gap-8">
            <div className="xl:col-span-8">
              <div className="flex flex-col">
                {service.imgArray.length > 1 ? (
                  <Swiper
                    centeredSlides={true}
                    autoplay={{
                      delay: 6000,
                      disableOnInteraction: false,
                    }}
                    effect={'fade'}
                    speed={2000}
                    navigation={true}
                    modules={[Autoplay, Pagination, Navigation, EffectFade]}
                    className="w-full h-[14rem] sm:h-[20rem] lg:h-[26rem] overflow-hidden border border-gray-100">
                    {service.imgArray?.map((item: any, index: number) => (
                      <SwiperSlide key={index} className="overflow-hidden">
                        <Image
                          src={item}
                          alt="bg-hero"
                          className={`h-full w-full`}
                          imageClassName="object-cover"
                        />
                      </SwiperSlide>
                    ))}
                  </Swiper>
                ) : (
                  <Image
                    src={service.imgArray[0]}
                    alt="bg-hero"
                    className="w-full h-[14rem] sm:h-[20rem] lg:h-[26rem] overflow-hidden border border-gray-100"
                    imageClassName="object-cover"
                  />
                )}
              </div>

              <h2 className="mt-8 text-3xl lg:text-4xl uppercase text-h2Color">
                {`${service.title}`}
              </h2>
              <p className="text-[16px] mt-4">{service.description}</p>

              <div className="flex mt-6">
                {service.twoImgsGallery.map((item: string, index: number) => (
                  <Image
                    key={index}
                    src={item}
                    alt="bg-hero"
                    className="w-full h-[14rem] sm:h-[16rem] lg:h-[20rem] overflow-hidden border border-gray-100"
                  />
                ))}
              </div>
              <div className="flex mt-8 flex-col">
                <h3 className="text-4xl text-h2Color">Our Capabilities</h3>
                <p className="text-[16px] mt-4">
                  {service.capabilities.description}
                </p>

                <div className="mt-8">
                  {service.capabilities.list.map(
                    (item: string, index: number) => (
                      <div className="flex flex-row items-center pt-4">
                        <span className="bg-[#f7f2ee] rounded-full h-6 w-6 flex justify-center items-center mr-3">
                          <FontAwesomeIcon
                            icon={faCheck}
                            className="text-xl text-white w-4 h-4"
                          />
                        </span>
                        <p className="text-[15px]">{item}</p>
                      </div>
                    )
                  )}
                </div>
              </div>
              <div className="flex mt-8 flex-col">
                <h3 className="text-4xl text-h2Color">Our approach</h3>
                <p className="text-[16px] mt-4">{service.approach}</p>
              </div>

              <div className="flex mt-8 flex-col">
                <h3 className="text-4xl text-h2Color">Our Work Process</h3>
                <p className="text-[16px] mt-4">
                  {service.capabilities.description}
                </p>
                <div className="mt-8">
                  {service.workProcess.map((item: string, index: number) => (
                    <div className="flex flex-row items-center pt-4">
                      <span className="bg-[#f7f2ee] rounded-full h-6 w-6 flex justify-center items-center mr-3">
                        <FontAwesomeIcon
                          icon={faCheck}
                          className="text-xl text-white w-4 h-4"
                        />
                      </span>
                      <p className="text-[15px]">{item}</p>
                    </div>
                  ))}
                </div>
              </div>
              <div className="flex mt-8 flex-col">
                <h3 className="text-4xl text-h2Color">Related Service</h3>
                <div className="flex flex-row  items-center pt-12 flex-wrap gap-5">
                  {services.slice(0, 3).map((item: any, index: number) => (
                    <Link href="/">
                      <div className="max-w-[307px]">
                        <Service
                          imageSrc={item.iconImg}
                          title={item.title}
                          description={item.shortDescription}
                          classes="shadow-3xl"
                        />
                      </div>
                    </Link>
                  ))}
                </div>
              </div>
            </div>

            <div className="xl:col-span-4">
              <ServiceSidebar {...{ service }} />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ServicePage;
