import Image from '@/components/Image';
import Link from 'next/link';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination, Navigation, EffectFade } from 'swiper/modules';
import { useEffect, useRef, useState } from 'react';
import PrymaryButton from '@/components/Buttons/Primary';

import {
  faArrowRightLong,
  faArrowLeftLong,
  faHome,
  faDotCircle,
  faCircleDot,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Hero = ({
  title,
  description,
  heroImg,
  hasBreadCrumb,
  secondLink,
  secondTitle,
  pageName,
}: any) => {
  const [activeSlide, setActiveSlide] = useState(-1);
  const firstTime = useRef(true);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setActiveSlide(0);
    }, 100);

    return () => clearTimeout(timeout);
  }, []);

  const handleSlideChange = (swiper: any) => {
    firstTime.current = false;
    setActiveSlide(swiper.activeIndex);
  };

  return (
    <section className="relative z-10 -mt-[104px]">
      <div className="relative isolate overflow-hidden">
        <Swiper
          centeredSlides={true}
          autoplay={{
            delay: 6000,
            disableOnInteraction: false,
          }}
          effect={'fade'}
          speed={2000}
          modules={[Autoplay, Pagination, Navigation, EffectFade]}
          className="inset-0 z-10 h-full w-full hero-slider"
          onSlideChange={handleSlideChange}>
          <SwiperSlide className="overflow-hidden">
            <div className="w-full h-screen flex flex-col lg:min-h-[35rem] lg:max-h-[35rem] max-h-[35rem] min-h-[30rem]">
              <Image
                src={heroImg}
                alt="bg-hero"
                className={`h-full w-full flex-1`}
                imageClassName="object-cover"
              />
              <div className="absolute left-0 top-0 bottom-0 z-10 w-full bg-[#040128] bg-opacity-60" />
              <div className={`absolute inset-0 z-20 flex items-center max-w`}>
                <div
                  className={`container relative flex items-center text-center`}>
                  <div className={`w-full`}>
                    <h1
                      className={`text-white text-3xl lg:text-6xl pt-20 pb-4 xl:pb-10`}>
                      {title}
                    </h1>
                    {hasBreadCrumb && (
                      <div className="mb-4 flex gap-2 whitespace-nowrap text-sm lg:text-lg justify-center text-white">
                        <Link
                          href="/"
                          className="flex gap-2 group items-center">
                          <div className="group-hover:text-primary transition-all duration-300">
                            Home
                          </div>
                        </Link>
                        <span className="flex items-center justify-center">
                          <FontAwesomeIcon
                            icon={faCircleDot}
                            className="text-white w-2 h8"
                          />
                        </span>

                        {secondLink && secondTitle && (
                          <>
                            <Link
                              href={secondLink}
                              className="flex gap-2 group items-center">
                              <div className="group-hover:text-primary transition-all duration-300">
                                {secondTitle}
                              </div>
                            </Link>
                            <span>/</span>
                          </>
                        )}

                        <div className="text-primary">{pageName}</div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </section>
  );
};

export default Hero;
