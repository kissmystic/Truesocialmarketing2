import Input from '@/components/input';
import { useToast } from '@/components/ui/use-toast';
import { yupResolver } from '@hookform/resolvers/yup';
import axios from 'axios';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import ButtonPrimary, { LinkType } from '@/components/Buttons/Primary';
import Select from '@/components/select';
import services from '@/constants/services';

const schema = yup.object().shape({
  name: yup.string().required('The name field is required.'),
  email: yup
    .string()
    .email('Invalid email')
    .required('The email field is required.'),
  phoneNumber: yup.string().required('The phone field is required.'),
  service: yup.string().required('Please select one service'),
  message: yup.string().required('The message field is required'),
});
const ContactInner = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { toast } = useToast();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    watch,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data: any) => {
    setIsLoading(true);

    try {
      const { name, email, phoneNumber, message, service } = data;

      const formData = new FormData();
      formData.append('_wpcf7_unit_tag', '8ced866');
      formData.set('your-subject', `New client: ${name} - ${service}`);
      formData.set('your-name', `${name}`);
      formData.set('service', `${service}`);
      formData.set('your-email', email);
      formData.set('phone', phoneNumber);
      formData.set('your-message', message);

      const response = await axios.post(
        process.env.NEXT_PUBLIC_CONTACT_FORM_URL!,
        formData
      );

      if (response.status === 200) {
        toast({
          title: 'Success',
          description:
            'Your request has been successfully submitted. One of our staff members will contact you shortly.',
          className: 'bg-green-400 text-white',
        });
        reset();
      }
    } catch (error) {
      toast({
        variant: 'destructive',
        title: 'Uh oh! Something went wrong.',
        description: 'There was a problem with your request. Please try later.',
      });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <form
      noValidate
      onSubmit={handleSubmit(onSubmit)}
      className="flex-1 flex flex-col shadow-3xl max-w-[69rem]">
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-6 py-12 px-12">
        <Input
          className="col-span-2 2xl:col-span-1 xl:col-span-1 md:col-span-1"
          id="name"
          label="Name"
          error={errors.name}
          {...register('name')}
          required
        />
        <Input
          className="col-span-2 2xl:col-span-1 xl:col-span-1 md:col-span-1"
          id="phoneNumber"
          label="Phone Number"
          {...register('phoneNumber')}
          value={watch('phoneNumber') || ''}
          phoneInput
          error={errors.phoneNumber}
          required
        />
        <Input
          className="col-span-2 2xl:col-span-1 xl:col-span-1 md:col-span-1"
          id="email"
          label="Email"
          {...register('email')}
          error={errors.email}
          required
        />
        <Select
          className="col-span-2 2xl:col-span-1 xl:col-span-1 md:col-span-1"
          options={[
            {
              value: '',
              label: 'Please select an service',
            },
            ...services.map((item) => ({
              value: item.title,
              label: item.title,
            })),
          ]}
          {...register('service')}
          error={errors.service}
          id="service"
          label="How Can We Help You?"
          placeholder="Please select"
          required
        />
        <Input
          className="col-span-2 sm:col-span-2"
          id="message"
          label="Message"
          {...register('message')}
          error={errors.message}
          textarea
          required
          rows={5}
        />
        <div className="flex justify-center items-center col-span-2">
          <ButtonPrimary
            type={LinkType.button}
            label={isLoading ? 'Submit...' : 'Submit Now'}
            style={`!text-sm !font-bold flex lg:flex !py-4 !px-11`}
          />
        </div>
      </div>
    </form>
  );
};

export default ContactInner;
