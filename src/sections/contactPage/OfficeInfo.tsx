import { faMapLocationDot } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { INFO } from '@/constants';

import OfficeItem from './OfficeItem';

export default () => (
  <div className="container">
    <div className="flex justify-center py-32">
      <div className="grid 2xl:grid-cols-3 xl:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-6">
        <OfficeItem
          icon={faMapLocationDot}
          title="Address"
          description={INFO.address}
          classes="shadow-3xl text-center"
        />

        <OfficeItem
          icon={faMapLocationDot}
          title="Email Us"
          description={INFO.email}
          classes="shadow-3xl text-center "
        />

        <OfficeItem
          icon={faMapLocationDot}
          title="Call Now"
          description={INFO.phone}
          classes="shadow-3xl text-center"
        />
      </div>
    </div>
  </div>
);
