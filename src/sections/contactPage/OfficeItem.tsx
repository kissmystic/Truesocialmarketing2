import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Service = ({ title, description, icon = <></>, classes = '' }: any) => {
  return (
    <div
      className={`px-24 py-12 bg-white flex justify-center items-center flex-col ${classes}`}>
      <span className="h-20 w-20 flex justify-center items-center mb-5 rounded-full bg-[#f5f5f5]">
        <FontAwesomeIcon
          icon={icon}
          className="text-xl text-regularColorOrange w-10 h-10"
        />
      </span>
      <h2 className="pb-3 font-medium text-3xl">{title}</h2>
      <p>{description}</p>
    </div>
  );
};

export default Service;
