import Hero from './Hero';
import ContactForm from './ContactForm';
import OfficeInfo from './OfficeInfo';

const ContactPage = () => {
  return (
    <section>
      <Hero
        title="Contacts Us"
        pageName="Contacts"
        hasBreadCrumb
        heroImg="/images/services/service1/2.jpg"
      />
      <OfficeInfo />
      <div className="flex flex-col justify-center items-center">
        <div className="max-w-md text-center">
          <h2 className="pb-5 font-normal">Have Any Question?</h2>
          <p className="pb-1">
            It is a long established fact that a reader will be distracted
            content of a page when looking.
          </p>
        </div>
      </div>
      <div className="container flex justify-center py-12">
        <ContactForm />
      </div>
    </section>
  );
};

export default ContactPage;
