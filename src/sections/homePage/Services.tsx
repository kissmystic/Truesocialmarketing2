import Image from '@/components/Image';
import motionVariants from '@/utils/motionVariants';
import { motion } from 'framer-motion';
import Service from '@/components/Service/Service';
import { faServicestack } from '@fortawesome/free-brands-svg-icons';
import { faPeopleCarry } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';

const Services = () => {
  return (
    <section className={`relative bg-[#0d1230] lg:pt-28 lg:pb-44 y-20`}>
      <div className="hidden shape-1 absolute inset-0 z-10 lg:flex">
        <Image
          className="!absolute h-full w-full  inset-0"
          imageClassName="flex object-cover"
          src="/images/services/6.png"
          alt="footer"
        />
      </div>
      <div className="container flex flex-col justify-center items-center">
        <div className="w-36 h-36 relative flex">
          <Image
            className="w-32 h-32"
            src="/images/services/1.png"
            alt="logo"
          />
          <div className="absolute bottom-14 text-regularColorOrange font-bold">
            WHY CHOOSE US?
          </div>
        </div>
        <h2 className="text-white -mt-10">Get All Facilities In One Station</h2>

        <div className="flex flex-row justify-center items-center gap-7 pt-12 flex-wrap">
          <Link href="/">
            <div className="max-w-[307px]">
              <Service
                imageSrc={'/images/services/2.png'}
                title="PLANNING"
                description="It seem that only fragment the original remain"
              />
            </div>
          </Link>
          <Link href="/">
            <div className="max-w-[307px]">
              <Service
                imageSrc={'/images/services/3.png'}
                title="FURNITURE"
                description="It seem that only fragment the original remain"
              />
            </div>
          </Link>
          <Link href="/">
            <div className="max-w-[307px]">
              <Service
                imageSrc={'/images/services/4.png'}
                title="FLOORS PLAN"
                description="It seem that only fragment the original remain"
              />
            </div>
          </Link>
          <Link href="/">
            <div className="max-w-[307px]">
              <Service
                imageSrc={'/images/services/5.png'}
                title="DECOR PLAN"
                description="It seem that only fragment the original remain"
              />
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Services;
