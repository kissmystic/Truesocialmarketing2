import Hero from './Hero';
import AboutUs from './AboutUs';
import Services from './Services';
import DreamBlock from './DreamBlock';

const HomePage = () => {
  return (
    <>
      <Hero />
      <AboutUs />
      <Services />
      <DreamBlock />
    </>
  );
};

export default HomePage;
