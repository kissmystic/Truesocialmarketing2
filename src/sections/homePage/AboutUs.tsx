import Image from '@/components/Image';
import motionVariants from '@/utils/motionVariants';
import { motion } from 'framer-motion';
import ButtonPrimary from '@/components/Buttons/Primary';

const AboutUs = () => {
  return (
    <section className={`lg:py-32 py-20`}>
      <div className="container">
        <div className="grid md:grid-cols-2">
          <motion.div
            initial="initial"
            whileInView="animate"
            viewport={{ once: true }}
            exit="exit"
            variants={motionVariants.fadeUp(40)}
            transition={{ duration: 0.5, delay: 0.1 }}
            className="
              md:mr-10
              lg:mr-15
              xl:mr-28
              relative 
              before:p-1 
              before:border-[1px] 
              before:border-white/60
              before:bg-transparent
              before:opacity-60 
              before:left-3
              before:right-3
              before:top-3
              before:bottom-3
              before:absolute 
              before:z-10
              before:pr-10 
              ">
            <Image
              src="/images/about/1.jpg"
              alt="about"
              className="h-full w-full lg:min-h-[15rem] min-h-[25rem]"
              imageClassName="object-cover"
            />
          </motion.div>
          <div className="relative py-8">
            <span className="relative pl-[80px] text-regularColorOrange text-lg before:content-[''] before:bg-regularColorOrange before:absolute before:h-[2px] before:left-0 before:top-[50%] before:w-[70px]">
              About Us
            </span>
            <h2
              className="
                font-bold 
                text-h2Color 
                mb-8 
                mt-5
                text-3xl 
                lg:text-5xl 
                pt-2 
                xl:pb-10
            ">
              We Offer You Profesional Interior Design
            </h2>
            <h5 className="text-h5Color text-xl italic font-normal  mb-8">
              Over 25 years Liarch helping investors building their drea &
              business goals go to the perfection
            </h5>
            <p className="text-pTextColore mb-10">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac enim
              aliquam feugiat ullamcorper. Id risus mattis neque, ullamcorper.
              Sed sit commodo vestibulum cras in cras. Nec proin scelerisque
              quis nisl vitae, egestas non. Fringilla auctor.
            </p>
            <ButtonPrimary
              href="#"
              label="Discover More"
              style={`!lg:text-sm !font-bold`}
            />
            <div className="w-14 h-14 rounded-full" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutUs;
