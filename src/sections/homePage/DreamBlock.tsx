import ButtonPrimary from '@/components/Buttons/Primary';

const DreamBlock = () => {
  return (
    <div className="container -mt-[4.5rem] mb-16 z-10">
      <span
        className="bg-regularColorOrange w-full flex justify-between items-center 
    !px-16 !py-12">
        <h2 className="text-white text-4xl">Make your dream property.</h2>
        <ButtonPrimary
          href="#"
          label="GET IN TOUCH"
          style="
          bg-white 
          !text-regularColorOrange 
          !text-sm 
          !font-bold
          hover:!bg-black 
          hover:!text-white 
          "
        />
      </span>
    </div>
  );
};

export default DreamBlock;
