import Image from '@/components/Image';
import Link from 'next/link';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination, Navigation, EffectFade } from 'swiper/modules';
import { useEffect, useRef, useState } from 'react';
import PrymaryButton from '@/components/Buttons/Primary';

import {
  faArrowRightLong,
  faArrowLeftLong,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const heroSliderImages = ['/images/hero/1.jpg', '/images/hero/2.jpg'];
const Hero = ({ title, description }: any) => {
  const [activeSlide, setActiveSlide] = useState(-1);
  const firstTime = useRef(true);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setActiveSlide(0);
    }, 100);

    return () => clearTimeout(timeout);
  }, []);

  const handleSlideChange = (swiper: any) => {
    firstTime.current = false;
    setActiveSlide(swiper.activeIndex);
  };

  return (
    <section className="relative z-10 -mt-[104px]">
      <div className="relative isolate overflow-hidden">
        <div className="hidden 2xl:flex arrow-left rounded-full bg-white/20 w-14 h-14 absolute z-20 top-1/2 left-5 bottom-0 items-center justify-center border-2 cursor-pointer">
          <FontAwesomeIcon
            icon={faArrowLeftLong}
            className={`text-xl text-white w-5 h-5`}
          />
        </div>
        <div className="hidden 2xl:flex arrow-left rounded-full bg-white/20 w-14 h-14 absolute z-20 top-1/2 right-5 bottom-0 items-center justify-center border-2 cursor-pointer">
          <FontAwesomeIcon
            icon={faArrowRightLong}
            className={`text-xl text-white w-5 h-5`}
          />
        </div>
        <Swiper
          centeredSlides={true}
          autoplay={{
            delay: 6000,
            disableOnInteraction: false,
          }}
          effect={'fade'}
          speed={2000}
          modules={[Autoplay, Pagination, Navigation, EffectFade]}
          className="inset-0 z-10 h-full w-full hero-slider"
          onSlideChange={handleSlideChange}
          navigation={{ nextEl: '.arrow-left', prevEl: '.arrow-right' }}>
          {heroSliderImages.map((item, index) => (
            <SwiperSlide key={index} className="overflow-hidden">
              <div className="w-full h-screen flex flex-col lg:min-h-[40rem] lg:max-h-[80rem] max-h-[50rem] min-h-[30rem]">
                <Image
                  src={item}
                  alt="bg-hero"
                  className={`h-full w-full flex-1`}
                  imageClassName="object-cover"
                />
                <div className="absolute left-0 top-0 bottom-0 z-10 w-full bg-[#040128] bg-opacity-60" />
                <div
                  className={`absolute inset-0 z-20 flex items-center max-w`}>
                  <div
                    className={`container relative flex items-center lg:justify-start lg:text-left md:text-left text-center`}>
                    <div className={`xl:w-3/5 md:w-2/3`}>
                      <h1
                        className={`text-white text-3xl lg:text-7xl pt-2 pb-4 xl:pb-10`}>
                        {title || 'Make your statement in style'}
                      </h1>
                      <div className="pt-2 pb-10">
                        <p className="text-lg lg:text-2xl text-white">
                          {description ||
                            `Lorem ipsum dolor sit amet sed, adipiscing elit, sed
                          do eiusmod tempor incididunt ut labore et dolore magna
                          aliqua ut enim ad.
`}
                        </p>
                      </div>
                      <PrymaryButton
                        href="/"
                        label="Explore More"
                        style="!font-bold"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </section>
  );
};

export default Hero;
