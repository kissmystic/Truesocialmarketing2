import Image from '@/components/Image';
import { formatDate } from '@/utils/formatDate';
import Link from 'next/link';

export default ({ otherPosts }: any) => {
  console.log(otherPosts);
  return (
    <div className="grid grid-cols-1 md:grid-cols-1 xl:grid-cols-1 gap-4 sm:gap-6">
      <div className="pl-4 pr-4 pb-4 lg:pl-8 lg:pr-8 lg:pb-8">
        <h2
          className="
            mb-4 
            relative
            sm:mb-6 
            text-2xl 
            before:rounded-xl 
            before:bottom-0 
            before:h-1 
            before:absolute 
            before:bg-regularColorOrange 
            before:left-0 
            before:w-14
            pb-5
            after:rounded-xl
            after:bottom-0
            after:h-1
            after:absolute
            after:left-16
            after:w-[80%]
          after:bg-[#f2f2f2]
        ">
          Related Posts
        </h2>
        <div>
          {otherPosts?.map((item: any, index: number) => (
            <div className="flex mb-8">
              <Link href={`/blog/${item?.slug}`} className="flex">
                <div key={index} className="pr-5 w-24 h-16 overflow-hidden">
                  <Image
                    imageClassName="object-cover"
                    className="h-full rounded-md overflow-hidden"
                    src={item?.featuredImage?.node?.sourceUrl}
                    alt="recent post"
                  />
                </div>
                <div className="flex flex-col flex-1">
                  <h4 className="pb-[0.5rem] text-h2Color text-base font-medium leading-5">
                    {item.title}
                  </h4>

                  <span className="flex-1 text-[#0d0925] text-xs">
                    {formatDate(item?.date) || ''}
                  </span>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
