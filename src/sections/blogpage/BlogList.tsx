import PostCard from './PostCard';
import { useEffect, useState } from 'react';
import Pagination from './Pagination';
import PageHead from '@/components/pageHead';
import Title from '@/components/Title';
import { staggerContainer } from '@/utils/motionVariants';
import { motion } from 'framer-motion';
import RelatedPosts from './RelatedPosts';
import SocialBlock from './SocialBlock';

const BlogList = ({
  posts,
  showItem = 10,
  showPagination = true,
  otherPosts,
}: any) => {
  const [currentPage, setCurrentPage] = useState(1);
  const showLimit = showItem,
    paginationItem = 10;

  const [pagination, setPagination] = useState<number[]>([]);
  const [limit, setLimit] = useState(showLimit);
  const [pages, setPages] = useState(Math.ceil(posts.length / limit));

  useEffect(() => {
    cratePagination();
  }, [limit, pages, posts.length]);

  const cratePagination = () => {
    let arr = new Array(Math.ceil(posts.length / limit))
      .fill(0)
      .map((_, idx) => idx + 1);

    setPagination(arr);
    setPages(Math.ceil(posts.length / limit));
  };

  const startIndex = currentPage * limit - limit;
  const endIndex = startIndex + limit;
  const getPaginatedProducts = posts.slice(startIndex, endIndex);

  let start = Math.floor((currentPage - 1) / paginationItem) * paginationItem;
  let end = start + paginationItem;
  const getPaginationGroup = pagination.slice(start, end);

  const scrollToTop = () => {
    window.scrollTo({ top: 300, behavior: 'smooth' });
  };

  const next = () => {
    setCurrentPage((page) => page + 1);
    scrollToTop();
  };

  const prev = () => {
    setCurrentPage((page) => page - 1);
    scrollToTop();
  };

  const handleActive = (item: number) => {
    setCurrentPage(item);
    scrollToTop();
  };

  return (
    <>
      <PageHead
        pageName="Blog"
        title="Latest News"
        hasBreadCrumb
        heroImg="/images/services/service1/2.jpg"
      />
      <section id="blog" className="padding-y">
        <div className="container">
          <div className="grid grid-cols-1 lg:grid-cols-9 2xl:gap-12 xl:gap-6 gap-4">
            <div className="col-span-7 md:col-span-6 flex-col">
              {getPaginatedProducts.map((item: any, index: number) => (
                <PostCard key={index} {...item} showDescription />
              ))}
              {showPagination && (
                <Pagination
                  getPaginationGroup={getPaginationGroup}
                  currentPage={currentPage}
                  pages={pages}
                  next={next}
                  prev={prev}
                  handleActive={handleActive}
                />
              )}
            </div>
            <div className="col-span-7 md:col-span-3 grid-cols-1 flex flex-col">
              <SocialBlock />
              <RelatedPosts {...{ otherPosts }} />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default BlogList;
