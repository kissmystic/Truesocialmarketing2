import {
  faFacebook,
  faInstagram,
  faTiktok,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import Image from 'next/image';
import { INFO } from '@/constants';
import { formatDate } from '@/utils/formatDate';
import PageHead from '@/components/pageHead';
import RelatedPosts from './RelatedPosts';
import PostCard from './PostCard';
import SocialBlock from './SocialBlock';

const shareSocialMeida = [
  {
    icon: faFacebook,
    name: 'facebook',

    href: (id: string) =>
      `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(
        `${INFO.site}/blog/${id}`
      )}`,
  },
  {
    icon: faTwitter,
    name: 'twitter',
    href: (id: string) =>
      `https://twitter.com/intent/tweet?url=${encodeURIComponent(
        `${INFO.site}/blog/${id}`
      )}`,
  },
  {
    icon: faInstagram,
    name: 'instagram',
    href: () => 'https://www.instagram.com/',
  },
  {
    icon: faTiktok,
    name: 'tiktok',
    href: () => 'https://www.tiktok.com/',
  },
];

const BlogPost = ({ post, otherPosts }: any) => {
  return (
    <>
      <PageHead
        pageName="Blog"
        title={post.title}
        hasBreadCrumb
        heroImg="/images/services/service1/2.jpg"
      />
      <article id="blog-post" className="padding-y">
        <div className="container">
          <div className="grid grid-cols-1 lg:grid-cols-9 gap-12">
            <div className="col-span-7 md:col-span-6 flex-col">
              <PostCard
                key={1}
                index={1}
                {...post}
                showDescription
                shortView={false}
              />
            </div>
            <div className="col-span-7 md:col-span-3 grid grid-cols-1">
              <SocialBlock />
              <RelatedPosts {...{ otherPosts }} />
            </div>
          </div>
        </div>
      </article>
    </>
  );
};
export default BlogPost;
