import Image from '@/components/Image';
import {
  faFacebook,
  faFacebookF,
  faLinkedin,
  faLinkedinIn,
  faPinterest,
  faPinterestP,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';

const SocialItem = ({ icon, href = '' }: any) => (
  <Link {...{ href }} className="group">
    <div className="bg-white overflow-hidden rounded-full flex items-center justify-center w-11 h-11">
      <FontAwesomeIcon
        icon={icon}
        className="text-black  w-4 h-4 group-hover:text-regularColorOrange"
      />
    </div>
  </Link>
);

export default () => {
  return (
    <div
      className="
        pl-2 pr-2 pb-4 
        2xl:pl-8 2xl:pr-8 2xl:pb-8
        xl:pl-8 xl:pr-8 xl:pb-8
        mb-12
        max-w-[25rem]
    ">
      <div className="bg-[#f1eae5] flex flex-col items-center justify-center px-11 py-11">
        <Image
          className="w-48 h-48 rounded-full overflow-hidden mb-6"
          imageClassName="w-48 h-48"
          src="/images/blog/socialPerson.jpg"
          alt="social buttons"
        />
        <h4 className="text-2xl text-h2Color font-normal mb-4">Jenny Watson</h4>
        <p className="text-center mb-[1.5rem]">
          Hi! beautiful people. I`m an authtor of this blog. Read our post -
          stay with us
        </p>
        <div className="grid grid-cols-4 gap-3">
          <SocialItem icon={faFacebookF} href="http://facebook.com" />
          <SocialItem icon={faTwitter} href="http://twitter.com" />
          <SocialItem icon={faLinkedinIn} href="http://linkedin.com" />
          <SocialItem icon={faPinterestP} href="http://pinterest.com" />
        </div>
      </div>
    </div>
  );
};
