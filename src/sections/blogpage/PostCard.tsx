import Image from '@/components/Image';
import { formatDate } from '@/utils/formatDate';
import { fadeIn } from '@/utils/motionVariants';
import {
  faArrowRight,
  faCalendar,
  faComments,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { motion } from 'framer-motion';

import Link from 'next/link';

const PostCard = ({
  title,
  date,
  slug,
  featuredImage,
  content,
  author,
  excerpt,
  shortView = true,
}: any) => {
  return (
    <div className="mb-20">
      <Image
        src={featuredImage?.node?.sourceUrl || '/images/no-image.jpg'}
        alt={`${title}-image`}
        className="max-h-[800px] h-[500px] overflow-hidden w-full"
        imageClassName="object-cover transition-all duration-500 group-hover:scale-[104%]"
      />

      <div className="flex-1 flex-col flex">
        <div className="py-8 flex flex-wrap items-baseline overflow-hidden text-sm leading-6">
          <div className="flex gap-3">
            <div className="flex items-center gap-3">
              <FontAwesomeIcon
                icon={faUser}
                className="h-3 text-black -mt-0.5"
              />
              <div className="text-sm uppercase">
                {author?.node?.name || ''}
              </div>
            </div>
            <div className="flex items-center gap-3 before:bg-regularColorOrange before:w-2 before:h-2 before:rounded-full">
              <FontAwesomeIcon
                icon={faComments}
                className="h-3 text-black -mt-0.5"
              />
              <div className="text-sm uppercase">COMMENTS 0</div>
            </div>
            <div className="flex items-center gap-3 before:bg-regularColorOrange before:w-2 before:h-2 before:rounded-full">
              <FontAwesomeIcon
                icon={faCalendar}
                className="h-3 text-black -mt-0.5"
              />
              <div className="text-sm uppercase">{formatDate(date)}</div>
            </div>
          </div>
        </div>
        <h3 className="font-semibold text-2xl text-black mb-6">
          <Link href={`/blog/${slug}`}>{title}</Link>
        </h3>

        <div
          dangerouslySetInnerHTML={{
            __html: shortView ? excerpt : content,
          }}
          className="mb-6 lg:prose-base text-[#484653] text-base"
        />

        {shortView && (
          <Link href={`/blog/${slug}`} className="flex gap-2 mt-auto">
            READ MORE...
          </Link>
        )}
      </div>
    </div>
  );
};

export default PostCard;
