import Hero from '@/sections/servicesPage/Hero';

type PageHeadProps = {
  title?: string;
  description?: string;
  hasBreadCrumb?: boolean;
  pageName?: string;
  className?: string;
  heroImg?: string;
};

const PageHead = ({
  heroImg,
  title,
  description,
  hasBreadCrumb = false,
  pageName,
  className,
}: PageHeadProps) => {
  return <Hero {...{ title, description, pageName, heroImg, hasBreadCrumb }} />;
};

export default PageHead;
