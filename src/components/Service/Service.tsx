import Image from '@/components/Image';

const Service = ({ title, description, imageSrc, classes = '' }: any) => {
  return (
    <div
      className={`px-10 py-12 bg-white flex items-center justify-center flex-col text-center ${classes}`}>
      <span className="bg-[#f7f2ee] rounded-full h-20 w-20 flex justify-center items-center mb-12">
        <Image className="w-10 h-10 relative" src={imageSrc} alt="logo" />
      </span>
      <h2 className="pb-3 font-normal">{title}</h2>
      <p>{description}</p>
    </div>
  );
};

export default Service;
