import Link from 'next/link';
import { MouseEventHandler } from 'react';

export enum LinkType {
  link = 'link',
  button = 'button',
}

export default ({
  type = LinkType.link,
  onClick = () => {},
  href = '#',
  label = '',
  style,
}: {
  type?: LinkType;
  onClick?: MouseEventHandler;
  href?: string;
  label: string;
  style: string;
}) => {
  if (type === LinkType.link) {
    return (
      <Link
        href={href}
        onClick={onClick}
        className={`
          lg:px-8
          lg:py-3.5
          px-8
          py-4
          inline-block
          text-white
          bg-regularColorOrange
          text-xl
          font-medium
          items-center
          hover:bg-white
          hover:text-black
          transition-all
          duration-300
          ${style}
      `}>
        {label}
      </Link>
    );
  }

  return (
    <button
      formAction="submit"
      onClick={onClick}
      className={`
        lg:px-8
        lg:py-3.5
        px-8
        py-4
        inline-block
        text-white
        bg-regularColorOrange
        text-xl
        font-medium
        items-center
        hover:bg-h2Color
        hover:text-regularColorOrange
        transition-all
        duration-300
        ${style}
    `}>
      {label}
    </button>
  );
};
