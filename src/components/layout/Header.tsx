import navigations from '@/constants/navigations';
import Image from '../Image';
import Link from 'next/link';
import Sidebar from './Sidebar';
import { INFO } from '@/constants';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import socialLinks from '@/constants/socialLinks';
import ButtonPrimary from '@/components/Buttons/Primary';

import { faSearchengin } from '@fortawesome/free-brands-svg-icons';

const Header = () => {
  return (
    <>
      <div
        className={`bg-header-top 
        w-full 
        z-20 
        relative 
        bg-topBar 
        before:content 
        before:h-full 
        before:left-full
        before:absolute 
        before:top-1/2 
        before:-translate-y-1/2 
        before:-translate-x-1/2
        before:w-2/3
        header-contact-clip-path
        overflow-hidden
      before:bg-white
        xl:before:block
        before:hidden
        `}>
        <div
          className={`
          relative
          container
          items-center py-1
          flex-wrap
          lg:justify-between
          flex
          justify-center
          text-nowrap
          `}>
          <ul
            className="flex 
            items-center 
            flex-wrap
            justify-center           
            flex-auto
            lg:justify-start
            pb-2.5
            ">
            <li className={`divider`}>
              <div className="flex text-xs">
                <p className={`text-[#ac8d68] font-bold`}>Email:</p>
                <p className={`flex items-center text-regularColorBlack`}>
                  &nbsp;{INFO.email}
                </p>
              </div>
            </li>
            <li className={`divider`}>
              <div className="flex text-xs">
                <p className={`text-[#ac8d68] font-bold`}>Phone:</p>
                <p className={`flex items-center text-regularColorBlack`}>
                  &nbsp;{INFO.phone}
                </p>
              </div>
            </li>
            <li className={`relative px-4 pt-2.5`}>
              <div className="flex text-xs">
                <p className={`text-[#ac8d68] font-bold`}>Location:</p>
                <p className={`flex items-center text-regularColorBlack`}>
                  &nbsp;{INFO.address}
                </p>
              </div>
            </li>
          </ul>
          <div className="gap-2 hidden lg:flex">
            {socialLinks.map((item) => (
              <Link
                key={item.name}
                href={item.href}
                className="text-sm leading-6 justify-center flex items-start gap-2"
                target="_blank">
                <FontAwesomeIcon
                  icon={item.icon}
                  className="w-7 h-7 opacity-40 text-slate-900 hover:text-regularColorOrange hover:opacity-100 transition-all duration-300 p-1"
                />
              </Link>
            ))}
          </div>
        </div>
      </div>
      <header>
        <div
          className={`
          header
          relative
          before:h-full 
          before:top-1/2 
          lg:before:w-1/3 
          md:before:w-1/4
          before:bg-white
          header-logo-clip-path
          before:absolute
          before:-translate-y-1/2
          before:hidden
          lg:before:block
          lg:bg-transparent
          lg:backdrop-blur-none
          bg-slate-600/10
          backdrop-blur-md
          z-20
          `}>
          <nav className="container py-4 xl:flex text-white text-base items-center flex justify-between">
            <div className="flex lg:hidden">
              <Sidebar />
            </div>
            <div className="flex px-4">
              <Link href="/">
                <Image
                  className="hidden lg:flex h-14 w-44"
                  src="/logo.svg"
                  alt="logo"
                />
                <Image
                  className="flex lg:hidden h-14 w-44"
                  src="/whiteLogo.svg"
                  alt="logo"
                />
              </Link>
            </div>
            <div className="hidden flex-1 items-center xl:gap-x-6 lg:gap-x-3 lg:flex justify-center">
              {navigations.map((item) => (
                <a
                  key={item.id}
                  href={item.href}
                  className="text-base  leading-6 text-white">
                  {item.title}
                </a>
              ))}
            </div>
            <div className="flex lg:gap-x-6 md:gap-x-1 sm:gap-x-1 justify-end">
              <Link
                href={'#'}
                className={`
                  lg:px-8 
                  lg:py-3.5
                  px-2.5 
                  py-2.5
                  lg:inline-block
                  text-white 
                  bg-regularColorOrange 
                  text-xl 
                  font-medium 
                  items-center 
                  hover:bg-white 
                  hover:text-black 
                  transition-all 
                  duration-300 
                `}>
                <FontAwesomeIcon
                  icon={faSearchengin}
                  className="w-5 h-5 opacity-40 text-white "
                />
              </Link>

              <ButtonPrimary
                href="#"
                label="BOOKING NOW"
                style={`!text-sm !font-bold hidden lg:flex`}
              />
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
