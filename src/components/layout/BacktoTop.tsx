import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import { faArrowUpLong } from '@fortawesome/free-solid-svg-icons';

const BacktoTop = () => {
  const [showArrow, setShowArrow] = useState(false);

  const handleToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  useEffect(() => {
    const handleArrow = () => {
      setShowArrow(window.scrollY > 300);
    };

    if (window) {
      handleArrow();

      window.addEventListener('scroll', handleArrow);

      return () => {
        window.removeEventListener('scroll', handleArrow);
      };
    }
  }, []);

  return (
    <>
      {showArrow && (
        <span
          className={`fixed flex w-11 h-11 right-3 bottom-3 rounded-full bg-[#db9e30] border-2 border-regularColorOrange items-center justify-center cursor-pointer z-30`}
          onClick={handleToTop}>
          <FontAwesomeIcon
            icon={faArrowUpLong}
            className="text-xl text-white w-4 h-4"
          />
        </span>
      )}
    </>
  );
};

export default BacktoTop;
