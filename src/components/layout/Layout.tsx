import BacktoTop from './BacktoTop';
import Footer from './Footer';
import Header from './Header';

type LayoutProps = {
  children: React.ReactNode;
};

const Layout = ({ children }: LayoutProps) => {
  return (
    <div className="flex flex-col min-h-screen">
      <Header />
      <main className="flex-grow flex flex-col min-h-[600px]">{children}</main>
      <Footer />
      <BacktoTop />
    </div>
  );
};

export default Layout;
