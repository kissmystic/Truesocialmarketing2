import {
  faLocationDot,
  faMailBulk,
  faMailForward,
  faPhone,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { INFO } from '@/constants';

import Link from 'next/link';

import Image from '../Image';
import { faMailchimp } from '@fortawesome/free-brands-svg-icons';

const Footer = () => {
  return (
    <footer className="relative bg-white overflow-hidden">
      <div className="absolute inset-0 bg-black/80 z-10" />
      <Image
        className="!absolute h-full w-full  inset-0"
        imageClassName="flex object-cover"
        src="/images/footer/1.jpg"
        alt="footer"
      />
      <div className="container relative flex lg:flex-row lg:justify-between flex-col items-start lg:py-32 py-16 z-10 text-white flex-wrap">
        <div className="flex flex-col lg:flex-1 max-w-sm lg:pb-0 pb-20">
          <Link href="/" className="flex pb-8">
            <Image className="h-14 w-44" src="/whiteLogo.svg" alt="logo" />
          </Link>
          <p className="text-white">
            Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Viverra
            laoreet ultrices donec placerat commodo elementum justo, consequat.
          </p>
        </div>
        <div className="flex lg:flex-1 justify-center lg:pb-0 max-w-sm pb-20">
          <div className="flex flex-col">
            <h3 className="pb-8">Our Services</h3>
            <Link href="/">
              <span>Floor Plans</span>
            </Link>
            <Link href="/" className="pt-4">
              <span>FURNITURE</span>
            </Link>
            <Link href="/" className="pt-4">
              <span>PLANNING</span>
            </Link>
            <Link href="/" className="pt-4">
              <span>Decor Plan</span>
            </Link>
            <Link href="/" className="pt-4">
              <span>FURNITURE</span>
            </Link>
          </div>
        </div>
        <div className="flex lg:flex-1 justify-center lg:pb-0 max-w-sm pb-20">
          <div className="flex flex-col">
            <h3 className="pb-8">Contact</h3>
            <span className="flex pb-4">
              <span className="pr-3 relative flex justify-center items-center">
                <FontAwesomeIcon
                  icon={faLocationDot}
                  className="w-5 h-5 text-white"
                />
              </span>
              <span>{INFO.address}</span>
            </span>
            <span className="flex pb-4">
              <span className="pr-3 relative flex justify-center items-center">
                <FontAwesomeIcon
                  icon={faPhone}
                  className="w-5 h-5 text-white"
                />
              </span>
              <span>{INFO.phone}</span>
            </span>
            <span className="flex pb-4 ">
              <span className="pr-3 relative flex justify-center items-center">
                <FontAwesomeIcon
                  icon={faMailchimp}
                  className="w-5 h-5 text-white"
                />
              </span>
              <span>{INFO.email}</span>
            </span>
          </div>
        </div>
        <div className="flex lg:flex-1 justify-end lg:pb-0 max-w-sm pb-20">
          <div className="flex flex-col">
            <h3 className="pb-8">Our Gellery</h3>
            <div className="flex ">
              <Image
                imageClassName="p-1"
                className="h-24 w-24"
                src="/images/footer/gallery/1.jpg"
                alt="footer"
              />
              <Image
                imageClassName="p-1"
                className="h-24 w-24"
                src="/images/footer/gallery/2.jpg"
                alt="footer"
              />
              <Image
                imageClassName="p-1"
                className="h-24 w-24"
                src="/images/footer/gallery/3.jpg"
                alt="footer"
              />
            </div>
            <div className="flex ">
              <Image
                imageClassName="p-1"
                className="h-24 w-24"
                src="/images/footer/gallery/4.jpg"
                alt="footer"
              />
              <Image
                imageClassName="p-1"
                className="h-24 w-24"
                src="/images/footer/gallery/5.jpg"
                alt="footer"
              />
              <Image
                imageClassName="p-1"
                className="h-24 w-24"
                src="/images/footer/gallery/6.jpg"
                alt="footer"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="relative z-20 border-t border-white/30">
        <div className="container">
          <div className="mt-8 md:order-1 md:mt-0 flex flex-row justify-between py-5">
            <p className="text-center leading-5 text-white text-base">
              &copy; {new Date().getFullYear()} {INFO.companyName}, Inc. All
              rights reserved.
            </p>
            <p className="text-center leading-5 text-white text-base">
              Privacy Environmental Policy
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
