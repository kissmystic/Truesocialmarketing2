import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from '@/components/ui/sheet';
import { INFO } from '@/constants';
import navigations from '@/constants/navigations';
import socialLinks from '@/constants/socialLinks';
import formatPhone from '@/utils/formatPhone';
import {
  faBarsStaggered,
  faClose,
  faEnvelope,
  faLocationDot,
  faPhoneVolume,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
// import Image from "../image";

const CustomSheetClose = (props: any) => {
  return (
    <SheetClose asChild {...props}>
      <div className="border-[1px] border-white px-2 py-2 absolute right-6 top-6 flex justify-center items-center w-10 h-10">
        <FontAwesomeIcon
          icon={faClose}
          className="transition-all duration-300 text-xl cursor-pointer w-6 rounded-full p-1 top-4 right-4"
        />
      </div>
    </SheetClose>
  );
};

const Sidebar = () => {
  return (
    <Sheet>
      <SheetTrigger asChild>
        <FontAwesomeIcon
          icon={faBarsStaggered}
          className="transition-all duration-300 text-xl cursor-pointer text-white bg-regularColorOrange rounded-sm p-2 w-[40px] h-[40px]"
        />
      </SheetTrigger>
      <SheetContent className="sheetContent !overflow-auto !w-1/2 bg-mobileSidebarBackground text-white">
        <SheetHeader className="mt-4">
          <SheetTitle className="text-left">
            <Link href="/" className="py-2 flex items-center gap-1">
              {/* <Image
                className="aspect-square h-16 invert"
                src="/logo.png"
                alt="logo"
              /> */}
            </Link>
          </SheetTitle>
        </SheetHeader>
        <CustomSheetClose />
        <div className="flex flex-col py-8 gap-2">
          {navigations.map((item) => (
            <Link
              key={item.id}
              href={item.href}
              className="text-sm font-normal leading-6 text-white py-4">
              {item.title}
            </Link>
          ))}
        </div>
      </SheetContent>
    </Sheet>
  );
};
export default Sidebar;
