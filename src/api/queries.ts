export const GET_BLOG_SLUGS = `
  query {
    posts(first: 1000) {
      nodes {
        slug
      }
    }
  }
`;

export const GET_BLOG_BY_SLUG = `
  query GetPostBySlug($slug: String) {
    postBy(slug: $slug) {
      date
      content
      id
      title
      slug
      author {
        node {
          name
        }
      }
      featuredImage {
        node {
          sourceUrl
        }
      }
      tags {
        nodes {
          name
        }
      }
    }
  }
`;

export const GET_RECENT_BLOG_POSTS = `
  query blogPosts {
    posts(first: 5) {
      nodes {
        id
        slug
        featuredImage {
          node {
            sourceUrl
          }
        }
        title
        content
        date
      }
    }
  }
`;

export const GET_BLOG_POSTS = `
  query blogPosts {
    posts(first: 100) {
      nodes {
        date
        content
        id
        title
        slug
        author {
          node {
            name
          }
        }
        featuredImage {
          node {
            sourceUrl
          }
        }
        excerpt
      }
    }
  }
`;

export const GET_FIRST_3_BLOGS = `
  query blogPosts {
    posts(first: 3) {
      nodes {
        id
        featuredImage {
          node {
            sourceUrl
          }
        }
        title
        content
        date
      }
    }
  }
`;
